const inquirer = require('inquirer')

console.log('Please specify the range you would like (>= 10)')

inquirer.prompt([
    {
        type: 'input',
        name: 'from',
        message: "From: ",
    },
    {
        type: 'input',
        name: 'to',
        message: "To: ",
    }
]).then(({ from, to }) => {
    from = from || 10
    to = to || 500

    if (from < 10)
        from = 10

    let numbers = []
    console.log(`Getting narcissistic numbers between ${from} and ${to}...`)

    for (let x = from; x <= to; x++) {
        let nos = x.toString().split('')
        let sum = nos.reduce((total, no) => total + Math.pow(no, nos.length), 0)
        
        if (sum === x)
            numbers.push(x)
    }

    console.log('Narcissistic numbers found:')
    console.log(numbers)
})